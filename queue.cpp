#include "queue.h"

void initQueue(queue* q, unsigned int size)
{
	q->arr = new int[size];
	q->size = size;
	q->index = 0;
}

void cleanQueue(queue* q)
{
	q->arr = nullptr;
	q->size = 0;
}

void enqueue(queue* q, unsigned int newValue)
{
	if (q->index < q->size)
	{
		q->arr[q->index] = newValue;
		q->index++;
	}
}

int dequeue(queue* q)// return element in top of queue, or -1 if empty
{
	int temp = 0;
	if (q->index > 0)
	{
		temp = q->arr[0];
		for (int i = 0; i < (q->index - 1); i++)
		{
			q->arr[i] = q->arr[i + 1];
		}

		q->index--;
		return temp;
	}
	return -1;
}